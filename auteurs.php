<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>citation</title>
</head>


<body>
    <header class="header">
        <nav class="navbar">
            <a href="#" class="nav-logo">Citation.fr</a>
            <li> <img class="logo" src="/image/logo.png" alt=""></li>
            <li><a href="backoffice.php">backoffice</a></li>
            <ul class="nav-menu">
                <li class="nav-item">
                    <a href="index.php" class="nav-link">Home</a>
                </li>
                <li class="nav-item">
                    <a href="auteurs.php"" class=" nav-link">Auteurs</a>
                </li>
                <li class="nav-item">
                    <a href="citations.php" class="nav-link">Citations</a>
                </li>
                <li class="nav-item">
                    <a href="citauteur.php" class="nav-link">Citation et son auteur</a>
                </li>
            </ul>
            <div class="hamburger">
                <span class="bar"></span>
                <span class="bar"></span>
                <span class="bar"></span>
            </div>
        </nav>
    </header>
    <div class="main">
        <h1>Auteurs</h1>

        <?php

        require 'connexion.php';
        $aut = $mysqli->query('SELECT * FROM Auteurs');

        foreach ($aut as $auteur) { ?>
            <div class="auth">
                <h4 class="prenom"> <?php echo $auteur['prenom'] ?></h4>
                <h4> <?php echo $auteur['Nom'] ?></h4>
            </div>
        <?php } ?>

    </div>
    <script src="script.js"></script>
</body>
<footer class="footer">
    <h5>©copyright 2022 citation.fr</h5>
</footer>

</html>