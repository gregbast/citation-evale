<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>citation</title>
</head>

<body>
    <header class="header">
        <nav class="navbar">
            <a href="#" class="nav-logo">Citation.fr</a>
            <li> <img class="logo" src="/image/logo.png" alt=""></li>
            <li><a href="backoffice.php">backoffice</a></li>
            <ul class="nav-menu">
                <li class="nav-item">
                    <a href="index.php" class="nav-link">Home</a>
                </li>
                <li class="nav-item">
                    <a href="auteurs.php"" class=" nav-link">Auteurs</a>
                </li>
                <li class="nav-item">
                    <a href="citations.php" class="nav-link">Citations</a>
                </li>
                <li class="nav-item">
                    <a href="citauteur.php" class="nav-link">Citation et son auteur</a>
                </li>
            </ul>
            <div class="hamburger">
                <span class="bar"></span>
                <span class="bar"></span>
                <span class="bar"></span>
            </div>
        </nav>
    </header>

    <div class="officeMain">
        <table class="table">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>nom</th>
                    <th>prenom</th>
                    <th>Citation</th>
                    <th>Supprimer</th>
                    <th>Modifier</th>

                </tr>
            </thead>

            <?php

            require 'connexion.php';
            $aut = $mysqli->query('SELECT * FROM Auteurs');

            foreach ($aut as $auteur) { ?>
                <tbody>
                    <tr>
                        <td>
                            <?php echo $auteur['id'] ?>
                        </td>
                        <td>
                            <?php echo $auteur['Nom'] ?>
                        </td>
                        <td>
                            <?php echo $auteur['prenom'] ?>
                        </td>
                        <td>
                            <?php echo $auteur['citation'] ?>
                        </td>
                        <td> <a class="supp" href="delete.php?ids=<?php echo $auteur['id']; ?>">Delete</a></td>
                        <td> <a class="supp" href="modif.php?id=<?php echo $auteur['id']; ?>">modifier</a></td>
                </tbody>
            <?php } ?>
        </table>
        <form method="post">
            Nom:<br>
            <input type="text" name="first_name">
            <br>
            Prenom:<br>
            <input type="text" name="last_name">
            <br>
            <textarea name="post_citation" id="" cols="30" rows="10"></textarea>
            <input type="submit" name="save" value="submit">
        </form>
        <?php
        require 'connexion.php';
        include_once 'connexion.php';
        if (isset($_POST['save'])) {
            $first_name = $_POST['first_name'];
            $last_name = $_POST['last_name'];
            $post_citation = $_POST['post_citation'];

            $sql = "INSERT INTO Auteurs (nom,prenom,citation)
	 VALUES ('$first_name','$last_name','$post_citation')";
            if (mysqli_query($mysqli, $sql)) {
                echo "New record created successfully !";
            } else {
                echo "Error: " . $sql . "
            " . mysqli_error($mysqli);
            }
            mysqli_close($mysqli);
        }
        //     
        ?>
        <?php
        //     require 'connexion.php';
        //     include_once 'connexion.php';
        //     if (isset($_POST['save'])) {
        //         $first_name = $_POST['first_name'];
        //         $last_name = $_POST['last_name'];
        //         $post_citation = $_POST['post_citation'];

        //         $sql = "INSERT INTO Citations (cit)
        //  VALUES ('$post_citation')";
        //         if (mysqli_query($mysqli, $sql)) {
        //             echo "New record created successfully !";
        //         } else {
        //             echo "Error: " . $sql . "
        //         " . mysqli_error($mysqli);
        //         }
        //         mysqli_close($mysqli);
        //     }

        //     
        ?>
    </div>
    <script src="script.js"></script>
</body>
<footer class="footer">
    <h5>©copyright 2022 citation.fr</h5>
</footer>

</html>