<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>citation</title>
</head>


<body>
    <header class="header">
        <nav class="navbar">
            <a href="#" class="nav-logo">Citation.fr</a>
            <li> <img class="logo" src="/image/logo.png" alt=""></li>
            <li><a href="backoffice.php">backoffice</a></li>
            <ul class="nav-menu">
                <li class="nav-item">
                    <a href="index.php" class="nav-link">Home</a>
                </li>
                <li class="nav-item">
                    <a href="auteurs.php"" class=" nav-link">Auteurs</a>
                </li>
                <li class="nav-item">
                    <a href="citations.php" class="nav-link">Citations</a>
                </li>
                <li class="nav-item">
                    <a href="citauteur.php" class="nav-link">Citation et son auteur</a>
                </li>
            </ul>
            <div class="hamburger">
                <span class="bar"></span>
                <span class="bar"></span>
                <span class="bar"></span>
            </div>
        </nav>
    </header>
    <div class="card">
        <div class="image">
            <img src="http://1.bp.blogspot.com/-EhPr4LXcywE/Udr594sPHTI/AAAAAAAAAJ4/Tv4y4CBLTPM/s400/Cristina-Otero-2.jpg" />
        </div>
        <div class="details">
            <div class="center">
                <h1>Karl Marx<br><span>Et ouai!!</span></h1>
                <p>Le domaine de la liberté commence là où s'arrête le travail déterminé par la nécessité </p>

            </div>
        </div>
    </div>
    <script src="script.js"></script>
</body>
<footer class="footer">
    <h5>©copyright 2022 citation.fr</h5>
</footer>

</html>