-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Feb 28, 2022 at 09:29 AM
-- Server version: 8.0.28-0ubuntu0.20.04.3
-- PHP Version: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `citation`
--

-- --------------------------------------------------------

--
-- Table structure for table `Auteurs`
--

CREATE TABLE `Auteurs` (
  `Nom` varchar(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `prenom` varchar(200) NOT NULL,
  `citation` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `Auteurs`
--

INSERT INTO `Auteurs` (`Nom`, `prenom`, `citation`, `id`) VALUES
('Guitry', 'Sacha', ' Rien n\'est plus facile à apprendre que la géométrie pour peu qu\'on en ait besoin.', 3),
('Shaw', 'George bernard', 'La règle d\'or, c\'est qu\'il n\'y a pas de règle d\'or.', 4),
('daniel', 'ghost', 'i im a ghost!!', 44),
('bob', 'dilan', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an u', 48),
('bob', 'dilan', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an u', 49),
('bill', 'boule', ' Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an u ', 50),
('bill', 'boule', ' Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an u ', 51),
('Marx', 'Karl', 'C beau la vie!ss', 57);

-- --------------------------------------------------------

--
-- Table structure for table `Citations`
--

CREATE TABLE `Citations` (
  `id_citation` int NOT NULL,
  `cit` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `Citations`
--

INSERT INTO `Citations` (`id_citation`, `cit`) VALUES
(8, 'tototot'),
(17, 'E=mc2'),
(18, 'E=mc2');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Auteurs`
--
ALTER TABLE `Auteurs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `Citations`
--
ALTER TABLE `Citations`
  ADD PRIMARY KEY (`id_citation`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Auteurs`
--
ALTER TABLE `Auteurs`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;

--
-- AUTO_INCREMENT for table `Citations`
--
ALTER TABLE `Citations`
  MODIFY `id_citation` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
