<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>citation</title>
</head>


<!-- <header>
    <nav class="nav">
        <ul class="nav-menu">
            <li>
                <h3>Citation.fr</h3>
            </li>
            <li><a href="backoffice.php">backoffice</a></li>
            <li> <img class="logo" src="/image/logo.png" alt=""></li>
            <li class="nav-item"><a href="index.php">Home</a></li>
            <li class="nav-item"><a href="auteurs.php">Auteurs</a></li>
            <li class="nav-item"><a href="citations.php">Citations</a></li>
            <li class="nav-item"><a href="citauteur.php">Citation et son auteur</a></li>
        </ul>
        <div class="hamburger">
            <span class="bar"></span>
            <span class="bar"></span>
            <span class="bar"></span>
        </div>
    </nav>

</header> -->


<body>
    <header class="header">
        <nav class="navbar">
            <a href="#" class="nav-logo">Citation.fr</a>
            <li> <img class="logo" src="/image/logo.png" alt=""></li>
            <li><a href="backoffice.php">backoffice</a></li>
            <ul class="nav-menu">
                <li class="nav-item">
                    <a href="index.php" class="nav-link">Home</a>
                </li>
                <li class="nav-item">
                    <a href="auteurs.php"" class=" nav-link">Auteurs</a>
                </li>
                <li class="nav-item">
                    <a href="citations.php" class="nav-link">Citations</a>
                </li>
                <li class="nav-item">
                    <a href="citauteur.php" class="nav-link">Citation et son auteur</a>
                </li>
            </ul>
            <div class="hamburger">
                <span class="bar"></span>
                <span class="bar"></span>
                <span class="bar"></span>
            </div>
        </nav>
    </header>
    <div class="car-citation">
        <?php
        require 'connexion.php';
        $sql = "SELECT * FROM Auteurs ORDER BY id DESC LIMIT 1";
        $query = mysqli_query($mysqli, $sql);
        $lastRow = mysqli_fetch_assoc($query);
        $lastID = $lastRow["id"];
        ?>

        <div class="card">
            <div class="image">
                <img src="http://1.bp.blogspot.com/-EhPr4LXcywE/Udr594sPHTI/AAAAAAAAAJ4/Tv4y4CBLTPM/s400/Cristina-Otero-2.jpg" />
            </div>
            <div class="details">
                <img class="logo3" src="/image/logo.png" alt="">
                <div class="center">
                    <h1><?php echo $lastRow['Nom'] ?> <?php echo $lastRow['prenom'] ?><br><span>citation du jour!</span></h1>
                    <p><?php echo $lastRow['citation'] ?></p>

                </div>
            </div>
        </div>
      


    </div>
    <script src="script.js"></script>
    <footer class="footer">
        <h5>©copyright 2022 citation.fr</h>
    </footer>
</body>

</html>