<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>Document</title>
</head>
<header>
    <nav class="nav">
        <ul>
            <li>
                <h3>Citation.fr</h3>
            </li>
            <li><a href="backoffice.php">backoffice</a></li>
            <li> <img class="logo" src="/image/logo.png" alt=""></li>
            <li><a href="index.php">Home</a></li>
            <li><a href="auteurs.php">Auteurs</a></li>
            <li><a href="citations.php">Citations</a></li>
            <li><a href="citauteur.php">Citation et son auteur</a></li>
        </ul>
    </nav>

</header>

<body>


    <div class="container">
        <?php


        require 'connexion.php';


        if (isset($_GET['id'])) {
            $id = (int) $_GET['id'];
            $res = $mysqli->query("SELECT * FROM Auteurs WHERE ID = " . $id);
            $row = $res->fetch_assoc();
            if (isset($row['citation'])) {
        ?>

                <form class="modText" method="POST">
                    <h1>Modifier</h1>
                    <input type=" text" value="<?php echo $row['prenom']; ?>" name="prenom">
                    <input type="text" value="<?php echo $row['Nom']; ?>" name="nom">
                    <input type="text" id="modText" value="<?php echo $row['citation']; ?>" name="citation">
                    <input type="submit" value="Modifier">
                </form>

        <?php }
        } ?>

        <?php


        if (isset($_POST['citation'])) {

            $citation = $_POST['citation'];
            $requete = 'UPDATE Auteurs SET citation="' . $citation . '" WHERE ID = ' . $id;
            $resultat = $mysqli->query($requete);
            if ($resultat) {
                header("Location:backoffice.php");
            } else {
                echo '<p class="error">Une erreur est survenue</p>';
            }
        }
        ?>
        <?php


        if (isset($_POST['nom'])) {

            $name = $_POST['nom'];
            $requete = 'UPDATE Auteurs SET Nom="' . $name . '" WHERE ID = ' . $id;
            $resultat = $mysqli->query($requete);
            if ($resultat) {
                header("Location:backoffice.php");
            } else {
                echo '<p class="error">Une erreur est survenue</p>';
            }
        }
        ?>
        <?php


        if (isset($_POST['prenom'])) {

            $prenom = $_POST['prenom'];
            $requete = 'UPDATE Auteurs SET prenom="' . $prenom . '" WHERE ID = ' . $id;
            $resultat = $mysqli->query($requete);
            if ($resultat) {
                header("Location:backoffice.php");
            } else {
                echo '<p class="error">Une erreur est survenue</p>';
            }
        }
        ?>
    </div>



</body>
<footer class="footer">
    <h5>©copyright 2022 citation.fr</h>
</footer>

</html>